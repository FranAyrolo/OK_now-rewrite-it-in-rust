#![allow(unused_parens)] 
#![allow(unused_imports)]
#![allow(dead_code)]
#![allow(non_snake_case)]
mod vec3;
mod color;
mod ray;
mod camera;
mod hittable;
mod hittable_list;
mod sphere;
mod constants;
use vec3::Vec3;
use color::Color;
use ray::Ray;
use sphere::Sphere;
use hittable::HitRecord;
use hittable_list::HittableList;


const ASPECT_RATIO: f32 = 16.0 / 9.0;
const IMAGE_WIDTH: i32 = 400;
const IMAGE_HEIGHT: i32 = (IMAGE_WIDTH as f32 / ASPECT_RATIO) as i32;

const FOCAL_LENGTH: f32 = 1.0;
const CAMERA_CENTER: Vec3 = Vec3{x: 0., y: 0., z: 0.};

const VIEWPORT_HEIGHT: f32 = 2.0;
const VIEWPORT_WIDTH: f32 = VIEWPORT_HEIGHT * (IMAGE_WIDTH as f32 / IMAGE_HEIGHT as f32);
const VIEWPORT_U: Vec3 = Vec3::from_ints(VIEWPORT_WIDTH as i32, 0, 0);
const VIEWPORT_V: Vec3 = Vec3::from_ints(0, -VIEWPORT_HEIGHT as i32, 0);

fn main() {
    println!("P3\n{} {} \n255", IMAGE_WIDTH, IMAGE_HEIGHT);
	
	let PIXEL_DELTA_U: Vec3 = VIEWPORT_U / IMAGE_WIDTH as f32;
	let PIXEL_DELTA_V: Vec3 = VIEWPORT_V / IMAGE_HEIGHT as f32;
	
	let VIEWPORT_UPPER_LEFT = CAMERA_CENTER - Vec3{x: 0., y: 0., z: FOCAL_LENGTH} - VIEWPORT_U/2. - VIEWPORT_V/2.;
    let PIXEL00_LOC = VIEWPORT_UPPER_LEFT + 0.5 * (PIXEL_DELTA_U + PIXEL_DELTA_V);
    
    
    let mut world: HittableList = HittableList::new();
    world.add(&Sphere{center: Vec3::from_ints(0, 0, -1), radius: 0.5});
    world.add(&Sphere{center: Vec3{x: 0., y: -100.5, z: -1.}, radius: 100.});
    
    //world.add(make_shared<sphere>(point3(0,0,-1), 0.5));
    //world.add(make_shared<sphere>(point3(0,-100.5,-1), 100));
    
    
	for j in 0..IMAGE_HEIGHT {
		for i in 0..IMAGE_WIDTH {
			let pixel_center: Vec3 = PIXEL00_LOC + (PIXEL_DELTA_U * i as f32) + (PIXEL_DELTA_V * j as f32);
			let ray_direction: Vec3 = pixel_center - CAMERA_CENTER;
            let r: Ray = Ray{origin: CAMERA_CENTER, direction: ray_direction};

            let pixel_color = camera::ray_color(&r, &world);
			Color::write_color(&pixel_color);
		}
	}
	
}
