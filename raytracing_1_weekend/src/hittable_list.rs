pub use crate::ray::Ray;
pub use crate::hittable::Hittable;
pub use crate::hittable::HitRecord;

pub struct HittableList<'a> {
	pub objects: Vec<&'a dyn Hittable>
}

impl HittableList<'_> {
	pub fn new() -> HittableList<'static>{
		HittableList{objects: Vec::new()}
	}
	
	pub fn clear(&mut self) {
		self.objects.clear();
	}
	
	pub fn add(&mut self, obj: &dyn Hittable) {
		self.objects.push(obj);
	}
	
	pub fn hit(&self, r: &Ray, ray_tmin: f32, ray_tmax: f32, rec: &mut HitRecord) -> bool {
		let temp_rec: HitRecord;
		let hit_anything: bool = false;
		let closest_so_far: f32 = ray_tmax;
		
		for obj in self.objects {
			if obj.hit(r, ray_tmin, closest_so_far, &mut temp_rec) {
				hit_anything = true;
				closest_so_far = temp_rec.t;
				rec = temp_rec;
			}
		}
		
		hit_anything
	}
}




//class hittable_list : public hittable {
//    hittable_list(shared_ptr<hittable> object) { add(object); }
//
//
//    void add(shared_ptr<hittable> object) {
//        objects.push_back(object);
//    }
//
//    bool hit(const ray& r, double ray_tmin, double ray_tmax, hit_record& rec) const override {
//        hit_record temp_rec;
//        bool hit_anything = false;
//        auto closest_so_far = ray_tmax;
//
//        for (const auto& object : objects) {
//            if (object->hit(r, ray_tmin, closest_so_far, temp_rec)) {
//                hit_anything = true;
//                closest_so_far = temp_rec.t;
//                rec = temp_rec;
//            }
//        }
//
//        return hit_anything;
//    }
//};
