pub use crate::vec3::Vec3;
pub use crate::ray::Ray;

pub struct HitRecord {
	pub p: Vec3,
	pub normal: Vec3,
	pub t: f32,
	pub front_face: bool
}

impl HitRecord {
	pub fn empty_hr() -> HitRecord {
		HitRecord{p: Vec3::ZERO(), normal: Vec3::ZERO(), t: 0., front_face: false}
	}
	
	pub fn set_face_normal(&mut self, r: &Ray, outward_normal: &Vec3) {
		// Sets the hit record normal vector.
		// NOTE: the parameter `outward_normal` is assumed to have unit length.
		self.front_face = Vec3::dot_product(&r.direction, outward_normal) < 0.;
		self.normal = if self.front_face {*outward_normal} else {Vec3::from_ints(0, 0, 0) - *outward_normal};	
	}
}

pub trait Hittable {
	fn hit(&self, r: &Ray, ray_tmin: f32, ray_tmax: f32, rec: &mut HitRecord) -> bool;
}
