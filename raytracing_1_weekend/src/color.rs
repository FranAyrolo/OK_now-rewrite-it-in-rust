pub use crate::vec3::Vec3;
pub use crate::vec3::Vec3 as Color;
pub use crate::ray::Ray;

impl Color {
	pub fn write_color(col: &Color) 
	{
		let r :f32 = col.x;
		let g :f32 = col.y;
		let b :f32 = col.z;
		
		let ir = (255.999 * r) as i32;
		let ig = (255.999 * g) as i32;
		let ib = (255.999 * b) as i32;
		
		println!("{} {} {}", ir, ig, ib);
	}
	
}//color
