#![allow(dead_code)]

#[derive (Debug, Clone, Copy)]
pub struct Vec3 {
	pub x: f32,
	pub y: f32,
	pub z: f32
}

impl Vec3 {
	pub const fn ZERO() -> Vec3 {
		Vec3{x: 0., y: 0., z: 0.}
	}
	
	pub const fn from_ints(_x :i32, _y: i32, _z: i32) -> Vec3{
		Vec3{x: _x as f32, y: _y as f32, z: _z as f32}
	}
	
	pub fn length_squared(&self) -> f32 
	{
		self.x*self.x + self.y*self.y + self.z*self.z
	}
	
	pub fn length(&self) -> f32 
	{
		self.length_squared().sqrt()
	}
	
	pub fn dot_product(u: &Vec3, v: &Vec3) -> f32
	{
		u.x*v.x + u.y*v.y + u.z*v.z
	}
	
	pub fn cross_product(u: &Vec3, v: &Vec3) -> Vec3
	{
		Vec3{
				x: u.y * v.z - u.z * v.y,
				y: u.z * v.x - u.x * v.z,
				z: u.x * v.y - u.y * v.x
			}
	}
	
	pub fn unit_vector(v: &Vec3) -> Vec3
	{
		Vec3{x: v.x / v.length(),
			 y: v.y / v.length(),
			 z: v.z / v.length()
			}
	}
}

impl std::ops::Add<Vec3> for Vec3{
	type Output = Vec3;
	fn add(self, _rhs: Vec3) -> Vec3{
		Vec3{x: self.x + _rhs.x, y: self.y + _rhs.y, z: self.z + _rhs.z}
	}
}

impl std::ops::Sub<Vec3> for Vec3{
	type Output = Vec3;
	fn sub(self, _rhs: Vec3) -> Vec3{
		Vec3{x: self.x - _rhs.x, y: self.y - _rhs.y, z: self.z - _rhs.z}
	}
}

impl std::ops::Mul<f32> for Vec3{
	type Output = Vec3;
	fn mul(self, _rhs: f32) -> Vec3{
		Vec3{x: self.x * _rhs, y: self.y * _rhs, z: self.z * _rhs}
	}
}

impl std::ops::Div<f32> for Vec3{
	type Output = Vec3;
	fn div(self, _rhs: f32) -> Vec3{
		Vec3{x: self.x / _rhs, y: self.y / _rhs, z: self.z / _rhs}
	}
}

//commutative of MUL
impl std::ops::Mul<Vec3> for f32{
	type Output = Vec3;
	fn mul(self, _rhs: Vec3) -> Vec3{
		Vec3{x: self * _rhs.x, y: self * _rhs.y, z: self * _rhs.z}
	}
}

//unary sub, for doing -Vec3
//impl std::ops::Neg<Output = Vec3> {
//	type Output = Vec3;
//	fn neg(self) -> Vec3 {
//		Vec3{x: -self.x, y: -self.y, z: -self.z}
//	}
//}
