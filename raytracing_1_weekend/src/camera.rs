//mod constants;
pub use crate::vec3::Vec3;
pub use crate::vec3::Vec3 as Color;
pub use crate::ray::Ray;
pub use crate::hittable::HitRecord;
pub use crate::hittable_list::HittableList;
use crate::constants;


fn hit_sphere(center: &Vec3, radius: f32, r: &Ray) -> f32 {
	//TODO better naming
	let oc: Vec3 = *center - r.origin;
	let a: f32 = r.direction.length_squared();
	let h: f32 = Vec3::dot_product(&r.direction, &oc);
	let c: f32 = oc.length_squared() - radius * radius;
	let discriminant: f32 = h*h - 4.*a*c;

	if discriminant < 0. {
        -1.0
    } else {
        (h - discriminant.sqrt()) / a
    }
	
}

pub fn ray_color(r: &Ray, world: &HittableList) -> Color {
	let mut rec: HitRecord = HitRecord::empty_hr();
	if world.hit(r, 0., constants::INFINITY, &mut rec) {
		return 0.5 * (rec.normal + Color{x: 1., y: 1., z: 1.});
	}
	
	/*
	//hit_record rec;
    //if (world.hit(r, 0, infinity, rec)) {
    //    return 0.5 * (rec.normal + color(1,1,1));
    //}
	
	//let t: f32 = hit_sphere(&Vec3::from_ints(0,0,-1), 0.5, r);
	//if t > 0. {
	//	let N: Vec3 = Vec3::unit_vector(&(r.at(t) - Vec3{x: 0., y: 0., z: -1.}));
    //    return 0.5 * Color{	x: N.x + 1., 
	//						y: N.y + 1., 
	//						z: N.z + 1.}
	//}
	*/
	let unit_direction: Vec3 = Vec3::unit_vector(&r.direction);
	let a: f32 = 0.5 * unit_direction.y + 1.0;
	
	(1.0 - a) * Color{x: 1.0, y: 1.0, z: 1.0} + a * Color{x: 0.5, y: 0.7, z: 1.0}
}
