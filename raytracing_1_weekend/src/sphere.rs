pub use crate::vec3::Vec3;
pub use crate::ray::Ray;
pub use crate::hittable::Hittable;
pub use crate::hittable::HitRecord;

pub struct Sphere {
	center: Vec3,
	radius: f32
}

impl Hittable for Sphere {
	fn hit(&self, r: &Ray, ray_tmin: f32, ray_tmax: f32, rec: &mut HitRecord) -> bool {
	//TODO better naming
		let oc: Vec3 = *self.center - r.origin;
		let a: f32 = r.direction.length_squared();
		let h: f32 = Vec3::dot_product(&r.direction, &oc);
		let c: f32 = oc.length_squared() - self.radius * self.radius;
		let discriminant: f32 = h*h - 4.*a*c;

		if discriminant < 0. {
			return false;
		}

		// Find the nearest root that lies in the acceptable range.
		let root: f32 = (h - discriminant.sqrt()) / a;
		if root <= ray_tmin || ray_tmax <= root {
			root = (h + discriminant.sqrt()) / a;
			if root <= ray_tmin || ray_tmax <= root {
				return false;
			}
		}
		
		rec.t = root;
		rec.p = r.at(rec.t);
		rec.normal = (rec.p - self.center) / self.radius;
		
		let outward_normal: Vec3 = (rec.p - self.center) / self.radius;
		rec.set_face_normal(&r, &outward_normal);
		
		true
	}
}

	


//bool hit(const ray& r, double ray_tmin, double ray_tmax, hit_record& rec) const override {
//        vec3 oc = center - r.origin();
//        auto a = r.direction().length_squared();
//        auto h = dot(r.direction(), oc);
//        auto c = oc.length_squared() - radius*radius;
//
//        auto discriminant = h*h - a*c;
//        if (discriminant < 0)
//            return false;
//
//        auto sqrtd = sqrt(discriminant);
//
//        // Find the nearest root that lies in the acceptable range.
//        auto root = (h - sqrtd) / a;
//        if (root <= ray_tmin || ray_tmax <= root) {
//            root = (h + sqrtd) / a;
//            if (root <= ray_tmin || ray_tmax <= root)
//                return false;
//        }
//
//        rec.t = root;
//        rec.p = r.at(rec.t);
//        rec.normal = (rec.p - center) / radius;
//
//        return true;
// }
