pub use crate::vec3::Vec3;
pub use crate::vec3::Vec3 as Point3;


#[derive (Debug)]
pub struct Ray {
	pub origin: Point3, 
	pub direction: Vec3
}

impl Ray {
	pub fn at(&self, t: f32) -> Vec3 {
		self.origin + self.direction * t
	}
}
